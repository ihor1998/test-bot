const { MongoClient, ObjectId } = require('mongodb');
const {
  MONGODB_CONNECTION_STRING,
  MONGODB_DATABASE_NAME,
  MONGODB_COLLECTION_BASKET,
  MONGODB_COLLECTION_PRODUCTS,
} = require('./config');

const client = new MongoClient(MONGODB_CONNECTION_STRING, {
  useUnifiedTopology: true,
});

async function connectToDatabase() {
  try {
    await client.connect();
    console.log('Соединение установлено!');

    const db = getDatabase();
    const collection = db.collection(MONGODB_COLLECTION_BASKET);

    await collection.deleteMany({});
  } catch (e) {
    console.error(e);
  }
}

function getDatabase() {
  return client.db(MONGODB_DATABASE_NAME);
}

async function getProducts(skip) {
  try {
    const db = getDatabase();
    const collection = db.collection(MONGODB_COLLECTION_PRODUCTS);

    return collection.find().skip(skip).limit(5).toArray();
  } catch (e) {
    console.error(e);
  }
}

async function getItemProduct(id) {
  try {
    const db = getDatabase();
    const collection = db.collection(MONGODB_COLLECTION_PRODUCTS);

    return await collection.findOne({ _id: ObjectId(id) });
  } catch (e) {
    console.error(e);
  }
}

async function createPurchase(id) {
  try {
    const db = getDatabase();
    const collection = db.collection(MONGODB_COLLECTION_BASKET);

    const item = await getItemProduct(id);

    await collection.insertOne(item);
  } catch (e) {
    console.error(e);
  }
}

async function getBasket() {
  try {
    const db = getDatabase();
    const collection = db.collection(MONGODB_COLLECTION_BASKET);

    return collection.find().toArray();
  } catch (e) {
    console.error(e);
  }
}

module.exports = {
  connectToDatabase,
  getDatabase,
  getProducts,
  createPurchase,
  getBasket,
};
