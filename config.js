const config = {
  MONGODB_CONNECTION_STRING: 'mongodb://localhost:27017/',
  MONGODB_DATABASE_NAME: 'testdb',
  MONGODB_COLLECTION_BASKET: 'basket',
  MONGODB_COLLECTION_PRODUCTS: 'products',
  MONGODB_COLLECTION_BUTTONS: 'buttons',
};

module.exports = config;
