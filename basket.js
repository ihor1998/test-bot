const sanitize = require('tg-md-sanitizer');
const { getBasket } = require('./database');
const {
  basketTextMessage,
  basketIsEmpty,
  checkoutTextMessage,
  checkoutButtonText,
} = require('./text');
const { createButton } = require('./buttons');

async function basketMessage(chatId, bot) {
  const basket = await getBasket();
  const message = basket.map((elem) => `${elem.title}\n`).join('');

  let text;
  let btn;
  if (basket.length !== 0) {
    text = basketTextMessage(message);
    btn = [
      [
        await createButton(checkoutButtonText('text'), {
          order: message,
          action: checkoutButtonText(),
        }),
      ],
    ];
  } else {
    text = basketIsEmpty();
    btn = [[]];
  }

  await bot.sendMessage(chatId, sanitize(text), {
    parse_mode: 'Markdown',
    reply_markup: { inline_keyboard: btn },
  });
}

async function checkoutMessage(chatId, name, message, bot) {
  const text = checkoutTextMessage(name, message);

  await bot.sendMessage(chatId, sanitize(text), { parse_mode: 'Markdown' });
}

module.exports = { basketMessage, checkoutMessage };
