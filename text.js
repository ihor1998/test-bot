const text = {
  sayHi: (name) => `Привет, ${name}! Я тестовый бот магазина!`,
  basketTextMessage: (message) =>
    `*Корзина:*\n =================\n _${message}_`,
  checkoutTextMessage: (name, message) =>
    `${name}, _Ваш заказ:_\n *${message}*\n Отправлен в обработку, ожидайте с Вами свяжутся!)`,
  basketIsEmpty: () => 'Ваша корзина пуста!',
  showProductsText: (title, description, img) =>
    `${title}\n ${description}\n ${img}\n`,
  purchaseButtonText: (option) => (option === 'text' ? 'Купить' : 'purchase'),
  basketButtonText: (option) => (option === 'text' ? 'Корзина' : 'basket'),
  checkoutButtonText: (option) =>
    option === 'text' ? 'Оформить заказ' : 'checkout',
  loadButtonText: () => 'Загрузить еще',
};

module.exports = text;
