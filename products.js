const sanitize = require('tg-md-sanitizer');
const { getProducts } = require('./database');
const {
  showProductsText,
  purchaseButtonText,
  basketButtonText,
  loadButtonText,
} = require('./text');
const { createButton } = require('./buttons');

async function showProducts(chatId, products_offset, bot) {
  const products = await getProducts(products_offset);

  for ([index, item] of products.entries()) {
    const title = sanitize(`*${item.title}*`);
    const description = sanitize(`_${item.description}_`);
    const img = sanitize(`[Link](${item.img})`);

    const text = showProductsText(title, description, img);

    const purchaseAndBasketBtn = [
      [
        await createButton(purchaseButtonText('text'), {
          product_id: item._id,
          action: purchaseButtonText(),
        }),
        await createButton(basketButtonText('text'), {
          action: basketButtonText(),
        }),
      ],
    ];

    const loadBtn = [
      [
        await createButton(loadButtonText(), {
          products_offset: products_offset + 5,
        }),
      ],
    ];

    const buttons =
      index !== products.length - 1
        ? purchaseAndBasketBtn
        : purchaseAndBasketBtn.concat(loadBtn);

    await bot.sendMessage(chatId, text, {
      parse_mode: 'Markdown',
      reply_markup: {
        inline_keyboard: buttons,
      },
    });
  }
}

module.exports = showProducts;
