const { ObjectId } = require('mongodb');
const { MONGODB_COLLECTION_BUTTONS } = require('./config');
const { getDatabase } = require('./database');

async function getButtonCallbackData(data) {
  const buttonData = { data };

  const db = getDatabase();
  const collection = db.collection(MONGODB_COLLECTION_BUTTONS);

  await collection.insertOne(buttonData);

  return buttonData;
}

async function getButtonData(id) {
  const db = getDatabase();
  const collection = db.collection(MONGODB_COLLECTION_BUTTONS);

  return await collection.findOne({ _id: ObjectId(id) });
}

async function createButton(text, callbackData) {
  const cbData = await getButtonCallbackData(callbackData);

  return { text: text, callback_data: cbData._id };
}

module.exports = { getButtonData, createButton };
