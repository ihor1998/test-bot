require('dotenv').config();

const TelegramBot = require('node-telegram-bot-api');
const { connectToDatabase, createPurchase } = require('./database');
const { getButtonData } = require('./buttons');
const { sayHi } = require('./text');
const { basketMessage, checkoutMessage } = require('./basket');
const showProducts = require('./products');

const token = process.env.TOKEN;
const bot = new TelegramBot(token, { polling: true });

connectToDatabase();

bot.on('message', async (msg) => {
  const { text } = msg;
  const chatId = msg.chat.id;
  const name = msg.chat.first_name;

  if (text === '/start') {
    return await bot.sendMessage(chatId, sayHi(name), {
      reply_markup: {
        keyboard: [[{ text: '📒 Каталог' }, { text: '🛒 Корзина' }]],
        resize_keyboard: true,
      },
    });
  }

  if (text === '📒 Каталог') {
    await showProducts(chatId, 0, bot);
  }

  if (text === '🛒 Корзина') {
    await basketMessage(chatId, bot);
  }
});

bot.on('callback_query', async (msg) => {
  const chatId = msg.message.chat.id;
  const name = msg.from.first_name;
  const id = msg.data;

  const { data } = await getButtonData(id);
  const { action, products_offset, order } = data;

  if (action === 'basket') {
    await basketMessage(chatId, bot);
  } else if (action === 'purchase') {
    await createPurchase(data.product_id);
  } else if (action === 'checkout') {
    await checkoutMessage(chatId, name, order, bot);
  } else {
    await showProducts(chatId, products_offset, bot);
  }
});
